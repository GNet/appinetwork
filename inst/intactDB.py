import sys
import os
import re
from zipfile import ZipFile
from urllib import request
import requests

try:
    path = sys.argv[sys.argv.index("intactDB.py")+1]
except:    
    sys.exit()
    
try:
    organism = sys.argv[sys.argv.index("intactDB.py")+2]
except:    
    sys.exit()

url = "https://www.ebi.ac.uk/legacy-intact/export?format=mitab_25&query=" + organism.replace('-', '+') + "&negative=false&spoke=false&ontology=false&sort=intact-miscore&asc=false"

response = request.urlretrieve(url, path + "/INTACT/INTACT-ORGANISM-" + organism + ".txt")

if os.path.getsize(path + "/INTACT/INTACT-ORGANISM-" + organism + ".txt") == 0:
    print("\n\nWarning : empty file")
