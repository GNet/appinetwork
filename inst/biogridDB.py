import sys
import os
from zipfile import ZipFile
from urllib import request
import requests

try:
    path = sys.argv[sys.argv.index("biogridDB.py")+1]
except:    
    sys.exit()
    
try:
    organism = sys.argv[sys.argv.index("biogridDB.py")+2]
except:    
    sys.exit()

organisme = organism.replace("-", "_")
organisme = "BIOGRID-ORGANISM-" + organisme
url = "https://downloads.thebiogrid.org/BioGRID/Release-Archive"
reponse = requests.get(url, stream = True)
version = ""
for line in reponse:
    for l in line.decode("utf-8").split('\n'):
        if "/BIOGRID" in l:
            version = l.split("/")[3]
            break  
    if version != "":
        break

v = version.split("-")[1]

url = "https://downloads.thebiogrid.org/Download/BioGRID/Release-Archive/BIOGRID-" + v + "/BIOGRID-ORGANISM-" + v + ".tab2.zip"

nomfichier = path + "/BIOGRID/BIOGRID-ORGANISM-" + v + ".tab2"

if os.path.exists(nomfichier + ".zip") == False:
  response = request.urlretrieve(url, nomfichier + ".zip")
  
name = ""
with ZipFile(nomfichier + ".zip") as myzip:
    name = myzip.namelist()
    for l in name:
        if organisme in l:
            myzip.extract(l, path)
            os.rename(path + "/" + l, path + "/BIOGRID/BIOGRID-ORGANISM-" + organism + ".txt")
            break
    myzip.close
