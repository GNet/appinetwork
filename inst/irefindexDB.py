'''
Version modifiee par JPHT
'''
import sys
import os
import re
from datetime import datetime
from zipfile import ZipFile
from urllib import request
import requests

# coucou
try:
    path = sys.argv[sys.argv.index("irefindexDB.py")+1]
except:    
    sys.exit()
    
try:
    organism = sys.argv[sys.argv.index("irefindexDB.py")+2]
except:    
    sys.exit()
    
try:
    typeOrganism = sys.argv[sys.argv.index("irefindexDB.py")+2]
except:    
    sys.exit()

if organism == "Saccharomyces-cerevisiae":
  organisme = "Saccharomyces-cerevisiae-s288c"
else:
  organisme = organism

### Recuperation de l'identifiant taxonomic (modif JPHT)
url = "https://www.ebi.ac.uk/ena/taxonomy/rest/scientific-name/" + organisme.replace('-', '+')
reponse = requests.get(url, stream = True)
taxID = reponse.json()[0]['taxId']     # c'est le taxid
# for line in reponse:
#     for l in line.decode("utf-8").split('\n'):
#         if "taxId" in l:
#             tax = l.split(":")[1]
#             break  
#     break
if typeOrganism == "Other":
    taxID = "All"
# else:
#     taxID = tax
# for i in re.findall("\d",tax):
#   taxID += i
  
url = "https://irefindex.vib.be/wiki/index.php/README_MITAB2.6_for_iRefIndex"
reponse = requests.get(url, stream = True)
version = ""
## nouveau code version JPHT
dateRelease = ""
for line in reponse:
    tabli = line.decode("utf-8").split('\n')
    for l in tabli :
        if "release:" in l:         #Applies to iRefIndex release: 19.0
            version = l.split(' ')[-1]
        elif "date:" in l:          # August 22nd, 2022 
            dateRelease = l.split(': ')[1]
        if version != "" and dateRelease != "":
            break

    if version != "" and dateRelease != "":
        break
## ancien code AL   
# for line in reponse:
#     for l in line.decode("utf-8").split('\n'):
#         if "release" in l:
#             version = l.split('_')[1]
#             version = version.split('/')[0]
#             break  
#     if version != "":
#         break
# reponse = requests.get(url, stream = True)
# date = {"January":"01","February":"02","March":"03","April":"04","May":"05","June":"06","July":"07","August":"08","September":"09","October":"10","November":"11","December":"12"}
# dateRelease = ""
# for line in reponse:
#     for l in line.decode("utf-8").split('\n'):
#         if "date" in l:
#             dateRelease = l.split(':')[1]
#             break
#     if dateRelease != "":
#         break
# d = dateRelease.split(' ')[2]
# day = ""
# for i in re.findall("\d",d):
#     day += i
# month = dateRelease.split(' ')[1]
# year = dateRelease.split(' ')[3]

#JPHT conversion de date
dtab = dateRelease.split(' ')
dtab[1] = ''.join(re.findall("\d",dtab[1]))
da = datetime.strptime(str(dtab),"['%B', '%d', '%Y']")

# endurl = taxID + ".mitab." + date[month] + "-" + day + "-" + year + ".txt"
# url = "https://storage.googleapis.com/irefindex-data/archive/release_" + version + "/psi_mitab/MITAB2.6/" + endurl
# nomfichier = "/IREFINDEX/IREFINDEX-ORGANISM-" + version + "-" + endurl

# JPHT modification des url et nom fichier
endurl = f"{taxID}.mitab.{da.strftime('%m-%d-%Y')}.txt"
url = f"https://storage.googleapis.com/irefindex-data/archive/release_{version}/psi_mitab/MITAB2.6/{endurl}" 
nomfichier = f"/IREFINDEX/IREFINDEX-ORGANISM-{version}-{endurl}"

myfichier = f"{path}{nomfichier}"
zipfich = f"{myfichier[:-3]}.zip"
renomorg = f"{path}/IREFINDEX/IREFINDEX-ORGANISM-{organism}.txt"

if os.path.exists(zipfich) == False:
    response = request.urlretrieve(url, myfichier)
    
    with ZipFile(zipfich, 'a') as myzip:
        myzip.write(myfichier, arcname = nomfichier[1:])
        os.rename(myfichier, renomorg)
        myzip.close
    print(f"\n\nIRefIndex file : {renomorg}")
    
elif os.path.exists(renomorg) == False:
    
    with ZipFile(zipfich, 'a') as myzip:
        myzip.extract(nomfichier[1:], path)
        os.rename(myfichier, renomorg)
        myzip.close
    print(f"\n\nIRefIndex file : {renomorg}")
    
else:
    print(f"\n\nIRefIndex file : {renomorg}")
    
if os.path.getsize(renomorg) == 0:
    print("\n\nWarning : empty file")



## ancien code AL 
# if os.path.exists(path + nomfichier[:-3] + '.zip') == False:
#     response = request.urlretrieve(url, path + nomfichier)
#
#     with ZipFile(path + nomfichier[:-3] + '.zip', 'a') as myzip:
#         myzip.write(path + nomfichier, arcname = nomfichier[1:])
#         os.rename(path + nomfichier, path + "/IREFINDEX/IREFINDEX-ORGANISM-" + organism + ".txt")
#         myzip.close
#     print("\n\nIRefIndex file : " + path + "/IREFINDEX/IREFINDEX-ORGANISM-" + organism + ".txt")
# elif os.path.exists(path + "/IREFINDEX-ORGANISM-" + organism + ".txt") == False:
#     with ZipFile(path + nomfichier[:-3] + '.zip', 'a') as myzip:
#         myzip.extract(nomfichier[1:], path)
#         os.rename(path + nomfichier, path + "/IREFINDEX/IREFINDEX-ORGANISM-" + organism + ".txt")
#         myzip.close
#     print("\nIRefIndex file : " + path + "/IREFINDEX/IREFINDEX-ORGANISM-" + organism + ".txt")
# else:
#     print("\nIRefIndex file : " + path + "/IREFINDEX/IREFINDEX-ORGANISM-" + organism + ".txt")
#
# if os.path.getsize(path + "/IREFINDEX/IREFINDEX-ORGANISM-" + organism + ".txt") == 0:
#     print("\n\nWarning : empty file")
