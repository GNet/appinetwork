# appinetwork
An R package for constructing and Analysing of Protein-Protein Interactions (PPI) NETWORKs for complexes and biological processes


## Installation

If you are on a **linux** os, We assume that R, Python 3 and C are installed, prior to any attempt to install the package appinetwork. 

To install this package from git, you need the devtools package using the following command in R :

    install.packages("devtools")
    library("devtools")
    
You also need the to install the following packages :

    install.packages( pkgs = c("R.methodsS3","rPython","stringr","rlang", "igraph"), dependencies = TRUE)

Now you can install our package from Gitlab using the following command in R :

    devtools::install_gitlab("GNet/appinetwork", host = "https://forgemia.inra.fr")


    
Installation on **Windows** and **MacOs** might be more difficult.

If you are on **windows**, you'll need to install [R](https://cran.r-project.org/bin/windows/base/) (we also recommend the installation of [RStudio](https://www.rstudio.com/products/rstudio/download/)), [python3](https://www.python.org/downloads/release/python-3105/) (**make sure to tick the "add python to path" option at the bottom of the installation window when you install it**) and [Rtools](https://cran.r-project.org/bin/windows/Rtools/rtools40.html) (the link is for R4.0 and up, for prior R version go to this [link](https://cran.r-project.org/bin/windows/Rtools/history.html)).
Then open Rstudio and enter the following commands : 
    
    install.packages("devtools")
    library("devtools")
    install.packages( pkgs = c("R.methodsS3","rPython","stringr","rlang", "igraph"), dependencies = TRUE, ask = FALSE)
    devtools::install_gitlab("GNet/appinetwork", host = "https://forgemia.inra.fr")

## Usage
Load the package with :
`library("appinetwork")`
This package implements a function `interface()` which enables to format PPI databses and performs two mains analysis : model complexes assembly intermediaries and cluster PPI networks. The step by step procedure is described in the [userguide](https://forgemia.inra.fr/GNet/appinetwork/-/blob/master/userguide_appinetwork.pdf).

## For more information : read the [userguide](https://forgemia.inra.fr/GNet/appinetwork/-/blob/master/userguide_appinetwork.pdf)

